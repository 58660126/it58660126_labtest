CREATE VIEW Registration AS
SELECT s.StudentID,s.StudentName AS StudentName,
       s.StudentSurname,
       c.CourseName AS CourseName,c.Credit AS Credit,
       r.Semester AS Semester

FROM Student AS s
LEFT JOIN ( Courses AS c,Regist  AS r)
ON ( c.CourseName = c.CourseID AND c.CourseName  = r.RegistID);
